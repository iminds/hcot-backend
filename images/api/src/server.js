const express = require("express");
const http = require("http");
const bodyParser = require("body-parser");
const cors = require("cors");
const retryKnex = require("./Utils/RetryKnex");
const uuidV1 = require("uuid/v1");
const moment = require("moment");
const Logs = require("./Fields/Logs");

class App {
  constructor(opts) {
    const pg = require("knex")({
      client: "pg",
      version: "9.6",
      connection: process.env.PG_CONNECTION_STRING,
      searchPath: ["knex", "public"]
    });

    this.pg = pg;
    this.start = this.start.bind(this);

    this.hasSetup = false;

    this.app = express();
    this.s = http.Server(this.app);
  }

  /** @inheritdoc */
  async start() {
    const _this = this;

    this.app.use(bodyParser.json());
    this.app.use(
      bodyParser.urlencoded({
        // to support URL-encoded bodies
        extended: true
      })
    );

    this.app.use(cors({ credentials: false, origin: "*" }));

    this.app.get("/", async (req, res, next) => {
      res.sendStatus(200);
    });

    this.app.get("/all", async (req, res, next) => {
      await this.pg
        .select("*")
        .table("measures")
        .orderBy("key", "desc")
        .then(d => {
          res.status(200).send(d);
        });
    });

    this.app.post("/remove/sensor", async (req, res, next) => {
      await this.pg
        .where({ sensorID: req.body.sensorID })
        .del()
        .table("measures")
        .then(d => {
          res.status(200).send(d);
        })
        .catch(e => res.status(400).send(e));
    });
    this.app.post("/remove/measureRange", async (req, res, next) => {
      await this.pg
        .where(key, "<", req.body.keyEnd)
        .andWhere(key, ">", req.body.keyStart)
        .del()
        .table("measures")
        .then(d => {
          res.status(200).send(d);
        })
        .catch(e => res.status(400).send(e));
    });

    this.app.post("/live", async (req, res, next) => {
      await this.pg
        .raw(
          `SELECT DISTINCT "sensorID"
          FROM measures;`
        )

        .then(async d => {
          const total = [];
          await d.rows.forEach(async e => {
            await this.pg
              .select("*")
              .table("measures")
              .where({ sensorID: e.sensorID })
              .limit(1)
              .orderBy("timestamp", "desc")
              .then(c => {
                if (c.length > 0) {
                  total.push(c[0]);
                }
                if (total.length == d.rows.length) {
                  const r = [];
                  total.map((index, key) => {
                    if (
                      parseInt(index.timestamp) < parseInt(req.body.end) &&
                      parseInt(index.timestamp) > parseInt(req.body.start)
                    ) {
                      console.log("match");
                      r.push(index);
                    } else {
                      if (
                        parseInt(moment(index.created_at).format("x")) <
                          parseInt(req.body.end) &&
                        parseInt(moment(index.created_at).format("x")) >
                          parseInt(req.body.start)
                      ) {
                        console.log("match");
                        r.push(index);
                      }
                    }
                  });
                  console.log(r);
                  res.status(200).send(r);
                }
              })
              .catch(e => {
                res.status(400).send(e);
              });
          });
        })
        .catch(e => {
          res.status(400).send(e);
        });
    });
    this.app.get("/live", async (req, res, next) => {
      await this.pg
        .raw(
          `SELECT DISTINCT "sensorID"
          FROM measures;`
        )

        .then(async d => {
          const total = [];
          await d.rows.forEach(async e => {
            await this.pg
              .select("*")
              .table("measures")
              .where({ sensorID: e.sensorID })
              .limit(1)
              .orderBy("key", "desc")
              .then(c => {
                if (c.length > 0) {
                  total.push(c[0]);
                }
                if (total.length == d.rows.length) {
                  res.status(200).send(total);
                }
              })
              .catch(e => {
                res.status(400).send(e);
              });
          });
        })
        .catch(e => {
          res.status(400).send(e);
        });
    });
    this.app.post("/", async (req, res, next) => {
      const uuid = uuidV1();

      const body = {
        sensorID: req.body.sensorID,
        decibelValue: req.body.decibelValue,
        decibelTimerange: req.body.decibelTimerange,
        timestamp:
          req.body.timestamp !== null
            ? req.body.timestamp
            : moment().format("x"),
        geohash: req.body.geohash,
        detected: req.body.detected,
        uuid: uuid
      };

      if (body.timestamp < 1000000000000) {
        body.timestamp = body.timestamp * 1000;
      }
      await this.pg
        .insert(body)
        .table("measures")
        .returning("*")
        .then(d => {
          res.status(200).send(d);
        });
    });

    this.app.post("/sensor/:id", async (req, res, next) => {
      await this.pg
        .select("*")
        .table("measures")
        .where({ sensorID: req.params.id })
        .then(d => {
          const r = [];
          d.map((index, key) => {
            if (index.timestamp) {
              if (
                parseInt(index.timestamp) < parseInt(req.body.end) &&
                parseInt(index.timestamp) > parseInt(req.body.start)
              ) {
                r.push(index);
              }
            } else {
              if (
                parseInt(moment(index.created_at).format("x")) <
                  parseInt(req.body.end) &&
                parseInt(moment(index.created_at).format("x")) >
                  parseInt(req.body.start)
              ) {
                r.push(index);
              }
            }
          });
          res.status(200).send(r);
        });
    });

    let version = process.env.APP_VERSION_NUMBER;
    if (process.env.APP_VERSION_LABEL) {
      version += "-" + process.env.APP_VERSION_LABEL;
    }

    console.info(`HCOT (${version}) ready and listening on ${3300}`);

    new Logs(this.broker).assignFields(this.app, this.pg);

    this.s.listen(3300, () => {
      console.log(`server up and listening on ${3300}`);
    });

    return await retryKnex(async () => {
      const self = this;

      await this.pg
        .raw("select 1+1 as result")
        .then(async (resolve, reject) => {
          if (!_this.hasSetup) {
            _this.initialiseTables(_this.pg);
            return true;
          }
        })
        .catch(error => {
          console.log("- error:", error.code);
          setTimeout(retryKnex(), 5000);
        });
    });
  }

  async initialiseTables(pg) {
    const _this = this;
    await this.pg.schema.hasTable("measures").then(function(exists) {
      if (!exists) {
        return _this.pg.schema
          .createTable("measures", function(table) {
            table.increments("key");
            table.uuid("uuid");
            table.string("sensorID");
            table.float("decibelValue");
            table.integer("decibelTimerange");
            table.bigInteger("timestamp");
            table.string("geohash");
            table.text("detected", "longtext");
            table.timestamps(true, true);
          })
          .then(function() {
            console.log("created measures");
          });
      }
    });

    await this.pg
      .select("*")
      .table("measures")
      .then(d => {
        const r = d.map(async (index, key) => {
          if (index.timestamp) {
            const timestamp = parseInt(index.timestamp);
            if (timestamp < 1000000000000) {
              await this.pg
                .update({ timestamp: timestamp * 1000 })
                .where({ sensorID: index.sensorID })
                .table("measures")
                .then(e => {
                  console.log("updated");
                })
                .catch(e => {
                  console.log(e);
                });
            }
          }
        });
      });
    // await this.pg.schema.hasTable("measures").then(function(exists) {
    //   if (exists) {
    //     return _this.pg.schema
    //       .alterTable("measures", function(table) {
    //         table.dropColumn("timestamp");
    //       })
    //       .then(function() {
    //         console.log("removed");
    //       });
    //   }
    // });
    // await this.pg.schema.hasTable("measures").then(function(exists) {
    //   if (exists) {
    //     return _this.pg.schema
    //       .alterTable("measures", function(table) {
    //         table.bigInteger("timestamp");
    //       })
    //       .then(function() {
    //         console.log("updated");
    //       });
    //   }
    // });

    // await this.pg.schema.hasTable("measures").then(function(exists) {
    //   if (exists) {
    //     return _this.pg.schema
    //       .alterTable("measures", function(table) {
    //         table.dropColumn("decibelTimerange");

    //       })
    //       .then(function() {
    //         console.log("removed");
    //       });
    //   }
    // });
    // await this.pg.schema.hasTable("measures").then(function(exists) {
    //   if (exists) {
    //     return _this.pg.schema
    //       .alterTable("measures", function(table) {
    //         table.integer("decibelTimerange");

    //       })
    //       .then(function() {
    //         console.log("updated");
    //       });
    //   }
    // });

    this.hasSetup = true;
  }
}

new App().start();
