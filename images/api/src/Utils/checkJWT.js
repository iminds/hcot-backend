const config = require('./../config.js');
const jwt = require('jsonwebtoken');

const checkJWT = async (req, cb, res) => {
  const token = req.headers.authorization.replace('Bearer ', '');
  jwt.verify(token, config.auth.secret, (err, decoded) => {
    if (err) {
      console.log(err);
      res.status(400).send(err);
      return false;
    } else {
      let sub = decoded.sub.split('|')[decoded.sub.split('|').length - 1];
      //@!94C6.B1CD.FACE.39C5!0001!6872.82DF!0000!EEC2.2512.2C88.951B
      sub = sub.replace(/\@/g, '');
      sub = sub.replace(/\!/g, '');
      sub = sub.replace(/\./g, '');
      sub = sub.toLowerCase();
      const user = {
        username: decoded.name,
        givenName: decoded.given_name,
        familyName: decoded.family_name,
        nickname: decoded.nickname,
        id: sub
      };
      // check if user is in database

      //vr7pk2lncdbivk7ti4bm5som8pdvaai8k77spkbsmdc

      cb(user);
    }
  });
};

module.exports = checkJWT;
