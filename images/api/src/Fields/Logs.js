const uuidV1 = require('uuid/v1');
const checkJWT = require('./../Utils/checkJWT');
const md5 = require('md5');

class Logs {
  constructor(broker, pg) {
    this.broker = broker;

    this.assignFields = this.assignFields.bind(this);
  }

  assignFields(app, pg) {
    const _this = this;

    app.get('/logs', async (req, res, next) => {
      await pg
        .select()
        .table('logs')
        .orderBy('created_at', 'DESC')
        .then((results) => {
          res.json(results);
        })
        .catch((error) => {
          res.status(400).send({ message: error });
        });
    });

    app.get('/logs/:device', async (req, res, next) => {
      await pg
        .select()
        .table('logs')
        .where('source', req.params.device)
        .orderBy('created_at', 'ASC')
        .then((results) => {
          res.json(results);
        })
        .catch((error) => {
          res.status(400).send({ message: error });
        });
    });

    app.post('/logs', async (req, res, next) => {
      const insert = req.body;
      insert['uuid'] = uuidV1();
      insert['data'] = JSON.stringify(req.body);
      await pg
        .insert(insert)
        .into('logs')
        .then(function(results) {
          res.json({ uuid: insert['uuid'] });
          // transfer to broker for matching
        })
        .catch((error) => {
          res.send(401, error);
        });
    });
  }
}

module.exports = Logs;
