const tailwindcss = require("tailwindcss");
const purgecss = require("@fullhuman/postcss-purgecss");
const cssnano = require("cssnano");
const autoprefixer = require("autoprefixer");

module.exports = {
  plugins: [
    tailwindcss("./tailwind.config.js"),
    process.env.NODE_ENV === "production" &&
      purgecss({
        content: [
          "./public/index.html",
          "./src/*.js*",
          "./src/**/*.js*",
          "./src/**/**/*.js*"
        ],
        css: ["./src/**/*.css", "./src/styles/*.css"],
        whitelist: [
          "focus:outline-none",
          "hover:opacity-100",
          "w-1/3",
          "w-1/6",
          "border-l",
          "border-r",
          "-mr-3",
          "-ml-2"
        ],
        whitelistPatternsChildren: [/blue$|green$|grey$/]
      }),
    cssnano({ preset: "default" }),
    autoprefixer
  ]
};
