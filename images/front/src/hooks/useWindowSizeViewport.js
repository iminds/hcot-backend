import { useEffect } from "react";

export function useWindowSizeViewport(handleViewportChange) {
  useEffect(() => {
    const handleResize = () => {
      handleViewportChange({
        height: window.innerHeight,
        width: window.innerWidth
      });
    };
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
      handleViewportChange({
        height: undefined,
        width: undefined
      });
    };
  }, []);
}
