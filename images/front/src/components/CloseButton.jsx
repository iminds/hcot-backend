import React from "react";

export default function CloseButton({ handleClose }) {
  return (
    <button
      className="absolute pin-t pin-r m-2 px-2 py-1 rounded text-bold text-grey-dark hover:text-black hover:bg-grey-lighter cursor-pointer focus:outline-none z-40"
      style={{ transition: "all .2s ease" }}
      onClick={handleClose}
    >
      ×
    </button>
  );
}
