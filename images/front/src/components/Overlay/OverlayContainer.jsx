import React, { useContext, useEffect, useState, useProps } from "react";
import moment from "moment";
import axios from "axios";
import Chart from "react-apexcharts";

export default function OverlayContainer({ sensor, timeRange, selectSensor }) {
  const [range, setRange] = useState(timeRange);
  const [data, setData] = useState([]);
  useEffect(
    () => {
      const CancelToken = axios.CancelToken;
      const source = CancelToken.source();
      const loadData = () => {
        const [start, end] = range;
        axios
          .post(
            `http://hcotsound.westeurope.cloudapp.azure.com:3300/sensor/${
              sensor.sensorID
            }`,
            {
              start,
              end
            }
          )
          .then(response => {
            const d = { name: sensor.sensorID, data: [] };

            const sorted = response.data
              .filter(a => a !== null)
              .sort((a, b) => parseInt(a.timestamp) - parseInt(b.timestamp))
              .forEach(index => {
                if (index) {
                  d.data.push({
                    x: moment(parseInt(index.timestamp)).format(),
                    y: index.decibelValue
                  });
                }
              });
            setData([d]);
          })
          .catch(err => console.log(err));
      };

      loadData();
      return () => {
        source.cancel();
      };
    },
    [timeRange]
  );

  const options = {
    fontFamily: "'Source Sans Pro' , Helvetica, Arial, sans-serif",
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      curve: "straight",
      lineCap: "butt",
      colors: undefined,
      width: 1.5,
      dashArray: 0
    },
    markers: {
      size: 2,
      strokeWidth: 0,
      strokeOpacity: 0.9,
      fillOpacity: 1,
      shape: "circle",
      radius: 2
    },

    yaxis: {
      min: 0,
      max: 140,
      tickAmount: 7
    },
    xaxis: {
      type: "datetime",
      // min: parseInt(timeSettings.range.start),
      // max: parseInt(timeSettings.range.end),
      labels: {
        datetimeFormatter: {
          year: "yyyy",
          month: "MMM 'yy",
          day: "dd MMM",
          hour: "dd/MM - HH:mm"
        },
        hideOverlappingLabels: true,
        showDuplicates: false
        // formatter: e => {
        //   return moment(e).format("DD/MM - HH:mm")
        // }
      },
      tooltip: {
        enabled: false
      }
    },
    chart: {
      animations: {
        enabled: false
      },
      toolbar: {
        show: true,
        autoSelected: "zoom"
      },
      id: "sensor-chart",
      type: "line"
    },
    colors: ["#45397F", "#4F5E97", "#6691AD", "#80C0C1", "#9CD3BE"],

    tooltip: {
      shared: true,
      x: {
        show: true,
        format: "dd MMM",
        formatter: date => {
          return moment(parseInt(date)).format("DD/MM/YYYY - HH:mm:ss");
        }
      },
      y: {
        formatter: val => {
          return `${Math.round(val * 100) / 100} dB`;
        }
      }
    }
  };

  const clickHandle = () => {
    selectSensor(null);
  };
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
        zIndex: 200,
        position: "fixed",
        display: "block"
      }}
      onClick={clickHandle}
    >
      <div
        style={{
          backgroundColor: "#fff",
          width: "400px",
          height: "620px",
          margin: "auto",
          marginTop: "100px",
          padding: "10px"
        }}
      >
        <div
          style={{
            padding: "0px 20px",
            boxSizing: "border-box",
            width: "100%"
          }}
        >
          <h1>{sensor.sensorID}</h1>
        </div>
        <div>
          {data.length > 0 && data[0].data.length > 0 ? (
            <>
              <Chart
                options={options}
                series={data}
                type="line"
                width="370"
                height="370px"
              />
              <div
                style={{
                  padding: "0px 30px",
                  boxSizing: "border-box",
                  width: "100%"
                }}
              >
                <table>
                  <tbody>
                    <tr>
                      <th style={{ width: "120px" }}>category</th>
                      <th>num. detected</th>
                    </tr>
                    <tr key={1}>
                      <td>Crying baby</td>
                      <td>{Math.round(Math.random() * 2)}</td>
                    </tr>
                    <tr key={2}>
                      <td>Car</td>
                      <td>{Math.round(Math.random() * 10)}</td>
                    </tr>
                    <tr key={3}>
                      <td>Bang</td>
                      <td>{Math.round(Math.random() * 2)}</td>
                    </tr>
                    <tr key={4}>
                      <td>Loud music</td>
                      <td>{Math.round(Math.random() * 7)}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </>
          ) : (
            <h1
              style={{
                display: "block",
                width: "100%",
                textAlign: "center",
                marginTop: "50px"
              }}
            >
              No data for this timerange
            </h1>
          )}
        </div>
      </div>
    </div>
  );
}
