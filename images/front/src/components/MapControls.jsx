import React, { useContext } from "react";
import { Store } from "store";
import { zoomIn, zoomOut } from "store/map";

export default function MapControls({ offsetBottom }) {
  const { dispatch } = useContext(Store);
  const handleZoomIn = () => dispatch(zoomIn());
  const handleZoomOut = () => dispatch(zoomOut());
  const buttonStyle =
    "bg-white py-2 px-4 text-grey-darker hover:bg-grey-light text-2xl font-bold shadow focus:outline-none";
  return (
    <div
      className="absolute pin-b pin-r my-4 mx-4 flex flex-col z-10"
      style={{ bottom: offsetBottom }}
    >
      <button
        className={`${buttonStyle} rounded-t`}
        style={{ transition: "all .2s ease" }}
        onClick={handleZoomIn}
      >
        +
      </button>
      <button
        className={`${buttonStyle} rounded-b`}
        style={{ transition: "all .2s ease " }}
        onClick={handleZoomOut}
      >
        −
      </button>
    </div>
  );
}
