import DeckGL, { MapView } from "deck.gl";
import React, { useContext } from "react";
import { Store } from "store";
import Spinner from "./Spinner";
import { getViewport } from "store/map";

export function Layers({ layers, error, loading, deckglOverlayId }) {
  const { state } = useContext(Store);
  const viewport = getViewport(state);
  if (loading) return <LoadingLayer />;
  const customDeckGLOverlayId = deckglOverlayId ? { id: deckglOverlayId } : {};
  return (
    layers && (
      <DeckGL
        {...customDeckGLOverlayId}
        views={[
          new MapView({
            controller: { doubleClickZoom: false }
          })
        ]}
        viewState={viewport}
        layers={layers}
      />
    )
  );
}

export const LoadingLayer = () => (
  <div className="w-full h-full flex items-center justify-center">
    <div className="rounded overflow-hidden absolute z-10">
      <Spinner dark={true} />
    </div>
  </div>
);
