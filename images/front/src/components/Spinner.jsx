import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Spinner({ dark = false }) {
  const darkClasses = "text-white bg-black";
  const lightClasses = "text-black bg-white";
  return (
    <div
      className={`w-full h-full items-center text-xl justify-center py-2 px-3 opacity-75 flex items-center justify-center cursor-default ${
        dark ? darkClasses : lightClasses
      }`}
    >
      <FontAwesomeIcon icon="spinner" className="fa-spin opacity-100" />
    </div>
  );
}
