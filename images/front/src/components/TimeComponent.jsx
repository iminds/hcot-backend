import React, { useState, useEffect } from "react";
import moment from "moment";
import { DatePicker } from "antd";
const { RangePicker } = DatePicker;
const dateFormat = "YYYY-MM-DD";

export default function TimeComponent({ handleChange, timeRange }) {
  const [data, setData] = useState([]);

  const onOk = value => {
    setData(value);
    handleChange([value[0].format("x"), value[1].format("x")]);
  };

  return (
    <div className="absolute py-4 px-4 pin-b pin-l inset-0 w-full flex jusitify-center items-center">
      <div
        className="bg-white px-4 py-4 rounded inline-block mx-auto"
        style={{ width: "400px" }}
      >
        <div className="w-full flex justify-center">
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="DD-MM-YYYY HH:mm"
            placeholder={["Start tijd", "Eind tijd"]}
            onChange={onOk}
            style={{ cursor: "pointer !important" }}
            allowClear={false}
            value={data}
            defaultValue={[
              moment(moment().subtract(1, "hour")),
              moment(moment())
            ]}
          />
        </div>
      </div>
    </div>
  );
}
