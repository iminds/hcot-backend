import React from "react";

const CloseIcon = ({ color = "black", size = 50 }) => {
  return (
    <svg
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={size}
      height={size}
      viewBox="0 0 357 357"
    >
      <g id="close">
        <polygon
          fill={color}
          points="351.4,40.2 316.8,5.6 178.5,143.9 40.2,5.6 5.6,40.2 143.9,178.5 5.6,316.8 40.2,351.4 178.5,213.1 316.8,351.4 
		351.4,316.8 213.1,178.5 	"
        />
      </g>
    </svg>
  );
};

export default CloseIcon;
