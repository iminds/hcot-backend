import React from "react";
import moment from "moment";

export default function ToolTipComponent({ sensor }) {
  return (
    <div
      className="bg-white p-4 m-2 z-50 absolute"
      style={{ minWidth: "250px", marginTop: "0px", marginLeft: "20px" }}
    >
      <div className="w-full overflow-hidden">
        {sensor.sensorID ? sensor.sensorID : "TestSensor (No ID)"}
      </div>
      {sensor.decibelValue !== null ? (
        <>
          <div className="w-full flex items-stretch">
            <div className="text-left p-1 flex-1">
              {moment(parseInt(sensor.timestamp)).format("DD/MM/YYYY")}
            </div>
            <div className="text-right p-1 flex-1">
              {moment(parseInt(sensor.timestamp)).format("HH:mm:ss")}
            </div>
          </div>
          <div className="text-xl p-1 font-bold">
            {sensor.decibelValue} Decibel
          </div>
          <div>
            <table style={{ width: "100%" }}>
              <tbody>
                <tr>
                  <th>Cat</th>
                  <th>Amount</th>
                </tr>
                <tr key={1}>
                  <td>{"car"}</td>
                  <td>5</td>
                </tr>
              </tbody>
            </table>
          </div>
        </>
      ) : (
        <div className="text-grey w-full">No data available</div>
      )}
    </div>
  );
}
