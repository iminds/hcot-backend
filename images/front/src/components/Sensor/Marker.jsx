import React, { useState, useContext } from "react";
import { Marker as MapBoxMarker } from "react-map-gl";
import { Store } from "store";
import Geohash from "latlon-geohash";

import Tooltip from "./TooltipComponent";

import { getSensorColor } from "utils/sensors";

export default function Marker({ sensor, selectSensor, num }) {
  const { dispatch, state } = useContext(Store);

  const [showTooltip, setShowTooltip] = useState(false);

  if (!sensor.geohash) return null;
  const { lon, lat } = Geohash.decode(sensor.geohash);

  if (!lat || !lon) return null;

  const color = getSensorColor(sensor.decibelValue);

  const onClick = () => {
    selectSensor(sensor);
  };

  return (
    <MapBoxMarker
      latitude={lat}
      longitude={lon}
      className={`${showTooltip ? "z-50" : "z10"}`}
    >
      <div
        style={{
          width: "22px",
          height: "22px",
          padding: "5px",
          position: "absolute",
          left: num * 10,
          zIndex: 10 - num
        }}
        onMouseEnter={() => setShowTooltip(true)}
        onMouseLeave={() => setShowTooltip(false)}
        onClick={() => onClick()}
      >
        <div
          className={`h-3 w-3 rounded-full mr-2 bg-black `}
          style={{
            backgroundColor: color
          }}
        />
      </div>
      {showTooltip ? <Tooltip sensor={sensor} /> : null}
    </MapBoxMarker>
  );
}
