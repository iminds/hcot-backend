import React from "react";
import SensorLegend from "./SensorLegend";
import MapControls from "components/MapControls";

export default function SensorComponents({ things }) {
  return (
    <>
      <SensorLegend />
      <MapControls />
    </>
  );
}
