import React from "react";
import { soundCompareTable } from "utils/sensors";

export default function SensorLegend() {
  return (
    <div
      className="absolute pin-b pin-l my-6 mx-4 bg-white rounded text-sm z-20"
      style={{ boxShadow: "0 0 2px 1px rgba(0, 0, 0, .1)", width: "250px" }}
    >
      <div className="w-full py-1 px-4 rounded-b z-10">
        <div className="w-full flex flex-col">
          {soundCompareTable.map((index, key) => {
            return (
              <div key={key} className="flex items-center block pt-1">
                <span
                  className="text-grey float-left text-right px-1"
                  style={{ width: "50px" }}
                >
                  {index.value} DB
                </span>
                <div
                  className="h-2 w-2 rounded-full mx-2 float-left"
                  style={{ backgroundColor: index.color }}
                />
                {index.comparison}
                <span className="text-black float-left px-1 overflow-hidden" />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
