import React, { useReducer, createContext } from "react";
import { combineReducers } from "redux";

import { initialState as mapInitialState, reducer as mapReducer } from "./map";
import {
  initialState as sensorInitialState,
  reducer as sensorReducer
} from "./sensor";

const reducer = combineReducers({
  map: mapReducer,
  sensor: sensorReducer
});

const initialState = {
  map: mapInitialState,
  sensor: sensorInitialState
};

// const customMiddleware = store => next => action => next(action)

export const Store = createContext({ state: initialState, dispatch: () => {} });

export function Provider({ children }) {
  const store = createStore(reducer, initialState);
  return <Store.Provider value={store}>{children}</Store.Provider>;
}

const compose = (...funcs) => x =>
  funcs.reduceRight((composed, f) => f(composed), x);

function createStore(reducer, initialState, middlewares) {
  // eslint-disable-next-line
  const [state, dispatch] = useReducer(reducer, initialState);
  if (typeof middlewares !== "undefined") {
    const middlewareAPI = {
      getState: () => state,
      dispatch: action => dispatch(action)
    };
    const chain = middlewares.map(middleware => middleware(middlewareAPI));
    const composeEnhancers =
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    // const composeEnhancers = compose
    const enhancedDispatch = composeEnhancers(...chain)(dispatch);
    return { state, dispatch: enhancedDispatch };
  }
  return { state, dispatch };
}
