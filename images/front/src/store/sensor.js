// ACTIONS
const SENSOR_SELECT = "sensor/SENSOR_SELECT";
const SENSOR_DESELECT = "sensor/SENSOR_DESELECT";
const SENSOR_DESELECT_ALL = "sensor/SENSOR_DESELECT_ALL";

// ACTION CREATORS
export const selectSensor = ({ sensorId }) => ({
  type: SENSOR_SELECT,
  payload: { sensorId }
});
export const deselectSensor = ({ sensorId }) => ({
  type: SENSOR_DESELECT,
  payload: { sensorId }
});
export const deselectAllSensors = () => ({ type: SENSOR_DESELECT_ALL });

// INIT STATE
export const initialState = {
  selected: []
};

// REDUCER
export function reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case SENSOR_SELECT: {
      const newSelected = state.selected;
      newSelected.push(payload.sensorId);
      return { ...state, selected: newSelected };
    }
    case SENSOR_DESELECT: {
      const newSelected = state.selected.filter(e => e !== payload.sensorId);
      return { ...state, selected: newSelected };
    }

    case SENSOR_DESELECT_ALL: {
      return { ...state, selected: [] };
    }
    default:
      return state;
  }
}

// SELECTORS
export function getSensors(state) {
  return state.sensor;
}
