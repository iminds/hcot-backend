import { FlyToInterpolator } from "react-map-gl";
import { easeCubic } from "d3-ease";

const defaultTransitionEffect = () => ({
  transitionDuration: 200,
  transitionInterpolator: new FlyToInterpolator(),
  transitionEasing: easeCubic
});

// ACTIONS
const RESET_VIEWPORT = "map/RESET_VIEWPORT";
const SET_VIEWPORT = "map/SET_VIEWPORT";
const ZOOM_IN = "map/ZOOM_IN";
const ZOOM_OUT = "map/ZOOM_OUT";

// ACTION CREATORS
export const resetViewport = ({ transition }) => ({
  type: RESET_VIEWPORT,
  payload: { transition }
});
export const setViewport = ({ viewport, bounds }) => ({
  type: SET_VIEWPORT,
  payload: { viewport, bounds }
});
export const zoomIn = () => ({ type: ZOOM_IN });
export const zoomOut = () => ({ type: ZOOM_OUT });

// INIT STATE
const [longitude, latitude] = [4.429812, 51.262394]; // Antwerp center
export const initialState = {
  bounds: {},
  viewport: {
    bearing: 0,
    latitude,
    longitude,
    pitch: 0,
    zoom: 11
  }
};

// REDUCER
export function reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case RESET_VIEWPORT: {
      const transition = payload.transition ? defaultTransitionEffect() : {};
      const {
        bearing,
        latitude,
        longitude,
        pitch,
        zoom
      } = initialState.viewport;
      return {
        ...state,
        viewport: {
          ...state.viewport,
          bearing,
          latitude,
          longitude,
          pitch,
          zoom,
          ...transition
        }
      };
    }
    case SET_VIEWPORT: {
      return {
        ...state,
        bounds: payload.bounds,
        viewport: {
          ...state.viewport,
          ...payload.viewport
        }
      };
    }
    case ZOOM_IN: {
      const transition = defaultTransitionEffect();
      return {
        ...state,
        viewport: {
          ...state.viewport,
          zoom: state.viewport.zoom + 1,
          ...transition
        }
      };
    }
    case ZOOM_OUT: {
      const transition = defaultTransitionEffect();
      return {
        ...state,
        viewport: {
          ...state.viewport,
          zoom: state.viewport.zoom - 1,
          ...transition
        }
      };
    }
    default:
      return state;
  }
}

// SELECTORS
export function getViewport(state) {
  return state.map.viewport;
}
export function getBounds(state) {
  return state.map.bounds;
}
