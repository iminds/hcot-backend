import React, { Suspense } from "react";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSpinner,
  faPalette,
  faPencilAlt,
  faCalendarAlt,
  faChartBar
} from "@fortawesome/free-solid-svg-icons";

import SensorMap from "./containers/SensorMap";
import Spinner from "./components/Spinner";

library.add(faSpinner, faPalette, faPencilAlt, faCalendarAlt, faChartBar);

export default function App() {
  return (
    <div className="bg-grey-light h-screen w-full">
      <Suspense fallback={<Spinner />}>
        <SensorMap default={true} path="sensors" title="HCOT" />
      </Suspense>
    </div>
  );
}
