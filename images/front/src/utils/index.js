import {
  findIndex as _findIndex,
  head as _head,
  last as _last,
  get as _get,
  uniq as _uniq,
  gt as _gt,
  isEqual as _isEqual,
  range as _range
} from "lodash";
import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import { TYPES } from "config";
// import { mockData } from "data/mockSimulationTimestamps"

dayjs.extend(isBetween);

/* MAP */

// Format longitude
const formatLng = lng => ((lng % 360) + 360) % 360;

// Get bounds
export function getBounds(mapEl) {
  if (!mapEl || !mapEl.current) return {};
  return mapEl.current.getMap().getBounds();
}

// Possible zooms 500, 100, 20
export function getTileSize(zoom) {
  if (zoom < 14 && zoom > 11.5) return 100;
  if (zoom >= 14) return 20;
  return 500;
}
// Get color out of the colorscale for the grid
export function getColorRange(value, colorScale) {
  if (value === _head(colorScale).range[0]) return 0;
  if (value < _head(colorScale).range[0]) return 0;
  if (value > _last(colorScale).range[1]) return colorScale.length - 1;
  const color = _findIndex(
    colorScale,
    ({ range: [lower, upper] }) => lower <= value && value < upper
  );
  return color;
}

export function isZoomout(viewport, minZoom) {
  return viewport.zoom < minZoom;
}

export function isInbound(bounds, bbox) {
  if (bounds === null || bbox === undefined) return false;
  const { _sw, _ne } = bounds;
  if (_sw === undefined || _ne === undefined) return false;
  const [minLng, maxLng] = [
    Math.min(bbox[0], bbox[2]),
    Math.max(bbox[0], bbox[2])
  ];
  const [minLat, maxLat] = [
    Math.min(bbox[1], bbox[3]),
    Math.max(bbox[1], bbox[3])
  ];
  if (_sw.lat > maxLat) return true;
  if (_ne.lat < minLat) return true;
  if (formatLng(_sw.lng) > formatLng(maxLng)) return true;
  if (formatLng(_ne.lng) < formatLng(minLng)) return true;
  return false;
}

/* RAIN / FLOOD */
export function parseData({ features }) {
  return features.map(({ type, properties, geometry }) => ({
    type,
    properties: {
      ...properties,
      FloodRisk: properties.FloodRisk * 100
    },
    geometry
  }));
}

export function handleHasData({ gridFeatures, loading, error }) {
  if (loading || error) return undefined;
  return _gt(_get(gridFeatures, "length", 0), 0);
}

export function getTimesteps(data, type, simulation_timestamp) {
  const warning = {};
  const simulationTS = simulation_timestamp;
  const isFlood = _isEqual(type, TYPES.flooding.type) ? ".catchments[0]" : "";
  const { reference, history, forecasts } = _get(
    data,
    `hydroscan.cities[0]${isFlood}.lastForecast`,
    {
      reference: [],
      history: [],
      forecasts: []
    }
  );
  // Uncomment this line to check with mock data (which you can edit)
  // const { reference, history, forecasts } = mockData
  const historyTimesteps = _uniq(history.map(({ timestamp }) => timestamp));
  const referenceTimesteps = _uniq(reference.map(({ timestamp }) => timestamp));
  const forecastsTimesteps = _uniq(forecasts.map(({ timestamp }) => timestamp));
  if (simulationTS === null) {
    if (checkTSisRealtime(referenceTimesteps[0]) === false) {
      Object.assign(warning, {
        message:
          "It's not realtime at the moment. We don't get any data for at last 1 hour."
      });
    }
    if (
      referenceTimesteps.length !== 1 ||
      historyTimesteps.length !== 36 ||
      forecastsTimesteps.length !== 36
    ) {
      Object.assign(warning, {
        message: "Missing some timestamps in the timeline."
      });
    }
    return {
      reference: referenceTimesteps,
      history: historyTimesteps,
      forecasts: forecastsTimesteps,
      warning
    };
  }
  const timesteps = {
    history: historyTimesteps,
    reference: referenceTimesteps,
    forecasts: forecastsTimesteps
  };
  const validTimesteps = checkTimestamps({
    referenceTimesteps,
    historyTimesteps,
    forecastsTimesteps,
    simulationTS
  });
  if (validTimesteps === true) return timesteps;
  if (validTimesteps.missing)
    Object.assign(warning, {
      message: `Missing timestamps: ${loopMissingTimestamps(
        validTimesteps.missing
      )}`
    });
  return { ...validTimesteps.timesteps, warning };
}

function loopMissingTimestamps(ts) {
  return ts.map(t => dayjs(t).format("HH:mm"));
}

function checkTimestamps({
  referenceTimesteps,
  historyTimesteps,
  forecastsTimesteps,
  simulationTS
}) {
  let history = _uniq(historyTimesteps);
  let reference = _uniq(referenceTimesteps);
  let forecasts = _uniq(forecastsTimesteps);

  /** Checking for historical data */
  const checkHistory = generatedDates
    .history(simulationTS)
    .map((timestamp, i) => {
      if (!dayjs(timestamp).isSame(dayjs(history[i]))) {
        const missingTimestamp = dayjs(timestamp)
          .subtract(2, "hours")
          .format("YYYY-MM-DDTHH:mm:00+00:00");
        history.splice(i, 0, missingTimestamp);
        return timestamp;
      }
      return true;
    });

  /** Checking for forecast data */
  const checkForecasts = generatedDates
    .forecast(simulationTS)
    .map((timestamp, i) => {
      if (!dayjs(timestamp).isSame(dayjs(forecasts[i]))) {
        const missingTimestamp = dayjs(timestamp)
          .subtract(2, "hours")
          .format("YYYY-MM-DDTHH:mm:00+00:00");
        forecasts.splice(i, 0, missingTimestamp);
        return timestamp;
      }
      return true;
    });
  /** Checking for reference data */
  const checkReference = dayjs(simulationTS).isSame(dayjs(reference[0]))
    ? true
    : roundDateNow();

  const timesteps = { history, reference, forecasts };
  const mergeChecks = checkHistory.concat(checkReference, checkForecasts);
  const filterMissingTimesteps = mergeChecks.filter(c => c !== true);
  if (filterMissingTimesteps.length)
    return { missing: filterMissingTimesteps, timesteps };
  return true;
}

function checkTSisRealtime(timestamp) {
  const isRealtime = dayjs(timestamp).isBetween(
    dayjs(),
    dayjs().subtract(1, "hour")
  );
  if (!isRealtime) return false;
  return true;
}

const generatedDates = {
  history(date) {
    const init = dayjs(date ? date : roundDateNow())
      .subtract(3, "hours")
      .format("YYYY-MM-DDTHH:mm:00+02:00");
    return _range(0, 36).map(i => {
      return dayjs(init)
        .add(5 * i, "minutes")
        .format("YYYY-MM-DDTHH:mm:00+02:00");
    });
  },
  forecast(date) {
    const init = dayjs(date ? date : roundDateNow())
      .add(5, "minutes")
      .format("YYYY-MM-DDTHH:mm:00+02:00");
    return _range(0, 36).map(i => {
      return dayjs(init)
        .add(5 * i, "minutes")
        .format("YYYY-MM-DDTHH:mm:00+02:00");
    });
  }
};

// Round date now to the nearest 10 minutes
function roundDateNow() {
  const coeff = 1000 * 60 * 5;
  const date = dayjs().valueOf();
  const rounded = dayjs(Math.round(date / coeff) * coeff);
  return dayjs(rounded)
    .subtract(10, "minutes")
    .format("YYYY-MM-DDTHH:mm:00+00:00");
}
