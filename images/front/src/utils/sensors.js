import { find as _find, get as _get } from "lodash";
import { decode } from "latlon-geohash";
import moment from "moment";

export function getSensorColor(db) {
  let color = "#666";
  if (db !== -1) {
    soundCompareTable.forEach(e => {
      if (db < e.value) {
        color = e.color;
      }
    });
  }
  return color;
}

export const soundCompareTable = [
  {
    comparison: "Vuurwerk",
    value: 140,
    color: "#A30A52",
    eval: "Pijngrens"
  },
  {
    comparison: "Straalmotor",
    value: 130,
    color: "#D80C6D",
    eval: "Extreem luid"
  },
  {
    comparison: "Politie sirene",
    value: 120,
    color: "#ED171E",
    eval: "Extreem luid"
  },
  {
    comparison: "Trombone",
    value: 110,
    color: "#F3431E",
    eval: "Extreem luid"
  },
  {
    comparison: "Helikopter",
    value: 100,
    color: "#F66F1B",
    eval: "Zeer luid"
  },
  {
    comparison: "Haardroger",
    value: 90,
    color: "#F99500",
    eval: "Zeer luid"
  },
  {
    comparison: "Vrachtwagen",
    value: 80,
    color: "#FFC300",
    eval: "Zeer luid"
  },
  {
    comparison: "Stadsverkeer",
    value: 70,
    color: "#FFE500",
    eval: "Luid"
  },
  {
    comparison: "Conversatie",
    value: 60,
    color: "#D8E100",
    eval: "Gematigd tot stil"
  },
  {
    comparison: "Regenval",
    value: 50,
    color: "#C0D912",
    eval: "Gematigd tot stil"
  },
  {
    comparison: "Koelkast",
    value: 40,
    color: "#73C13A",
    eval: "stil"
  },
  {
    comparison: "Gefluister",
    value: 30,
    color: "#14B046",
    eval: "stil"
  },
  {
    comparison: "Ritselende bladeren",
    value: 20,
    color: "#00A983",
    eval: "stil"
  },
  {
    comparison: "Ademhaling",
    value: 10,
    color: "#0098AA",
    eval: "Nauwelijks hoorbaar"
  },
  {
    comparison: "Minimale hoorgrens",
    value: 0,
    color: "#007CAB",
    eval: "Onhoorbaar"
  }
];
