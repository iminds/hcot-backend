import React, { useContext, useEffect, useState } from "react";
import useInterval from "@use-it/interval";
import moment from "moment";
import axios from "axios";

import FetchContainer from "containers/FetchContainer";
import OverlayContainer from "components/Overlay/OverlayContainer";
import Map from "containers/MapContainer";
import SensorComponents from "components/Sensor/SensorComponents";
import Marker from "components/Sensor/Marker";
import { MAP_PROPS } from "config";
import TimeComponent from "components/TimeComponent";
// import { Store } from "store"

let sensorMap = {};

export default function SensorMap({ title, sensors }) {
  const [data, setData] = useState([]);
  const [selectSensor, setSelectSensor] = useState(null);
  const [timeRange, setTimeRange] = useState([
    moment()
      .subtract(1, "day")
      .format("x"),
    moment().format("x")
  ]);

  return (
    <>
      <FetchContainer setData={setData} timeRange={timeRange}>
        <Map {...MAP_PROPS.sensor}>
          {data.map((thing, key) => {
            if (thing) {
              return (
                <Marker
                  key={key}
                  num={key}
                  sensor={thing}
                  selectSensor={setSelectSensor}
                />
              );
            }
          })}
          {selectSensor !== null ? (
            <OverlayContainer
              sensor={selectSensor}
              selectSensor={setSelectSensor}
              timeRange={timeRange}
            />
          ) : null}
        </Map>
        <SensorComponents />
        <TimeComponent handleChange={setTimeRange} timeRange={timeRange} />
      </FetchContainer>
    </>
  );
}
