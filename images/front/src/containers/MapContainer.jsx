import React, { useState, useEffect, useRef, useContext } from "react";
import ReactMapGL from "react-map-gl";
import { Store } from "store";
import "mapbox-gl/dist/mapbox-gl.css";
import { useWindowSizeViewport } from "hooks/useWindowSizeViewport";

import { setViewport, getViewport } from "store/map";
import { MAPBOX_TOKEN } from "config";
import { getBounds } from "utils";

export const Viewport = React.createContext({ viewport: {} });

export default function MapContainer({ children, zoom, latitude, longitude }) {
  const mapEl = useRef(null);
  const bounds = getBounds(mapEl);
  const [GL, setGL] = useState(null);
  const canvas = document.getElementById("deckgl-grid-overlay");
  useEffect(
    () =>
      dispatch(
        setViewport({
          viewport: { ...viewport, zoom, latitude, longitude },
          bounds
        })
      ),
    []
  );
  useEffect(() => {
    if (canvas) setGL(canvas.getContext("webgl2", {}));
    return () => {
      if (GL) {
        const extension = GL.getExtension("WEBGL_lose_context");
        if (extension) extension.loseContext();
      }
    };
  }, [canvas]);
  const { state, dispatch } = useContext(Store);
  const viewport = getViewport(state);
  const handleViewportChange = viewport =>
    dispatch(setViewport({ viewport, bounds }));
  useWindowSizeViewport(handleViewportChange);
  return (
    <ReactMapGL
      {...viewport}
      mapboxApiAccessToken={MAPBOX_TOKEN}
      onViewportChange={handleViewportChange}
      ref={mapEl}
      mapStyle={"mapbox://styles/imec-apt/ck18yj3qt0hq21cqece8wj5ve"}
    >
      {children}
    </ReactMapGL>
  );
}
