import React, { useContext, useEffect, useState, useProps } from "react";
import useInterval from "@use-it/interval";
import moment from "moment";
import axios from "axios";

// import { Store } from "store"

export default function FetchContainer({ timeRange, setData, children }) {
  const abortController = new AbortController();
  const [count, setCount] = useState(0);
  const [range, setRange] = useState(timeRange);
  useEffect(
    () => {
      const CancelToken = axios.CancelToken;
      const source = CancelToken.source();
      const loadData = () => {
        const [start, end] = timeRange;
        console.log(timeRange);
        axios
          .post(`http://hcotsound.westeurope.cloudapp.azure.com:3300/live`, {
            start,
            end
          })
          .then(response => {
            console.log(response.data);
            setData(response.data);
          })
          .catch(err => console.log(err));
      };

      setTimeout(() => setCount(count + 1), 5000);
      loadData();
      return () => {
        source.cancel();
      };
    },
    [timeRange, count]
  );
  return <>{children}</>;
}
