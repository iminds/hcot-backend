import "./index.css";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'

import React from "react";
import ReactDOM from "react-dom";
import { Provider as ReduxProvider } from "store";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <ReduxProvider>
    <App />
  </ReduxProvider>,
  document.getElementById("root")
);

serviceWorker.unregister();
