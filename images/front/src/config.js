// GENERAL CONFIGURATION
export const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;

export const DATETIME_FORMAT = "DD/MM/'YY - HH:mm";
export const DATE_FORMAT = "DD/MM/'YY";
export const POLL_INTERVAL = 1000 * 60; // 1 minute
export const SCREEN_WIDTH = {
  sm: 900,
  med: 1180
};

// testing purposes, until sensors move
export const MAP_PROPS = {
  sensor: {
    zoom: 12.5,
    latitude: 51.219447989,
    longitude: 4.402463988
  }
};

export const MAP_DATA_TIME_RANGE = {
  amount: 1,
  unit: "hour"
};

// RAINFALL & FLOODING CONFIGURATION
export const TYPES = {
  rainfall: { type: "rain", unit: "mm/h" },
  flooding: { type: "flood", unit: "cm" }
};
export const BOUNDING_BOX = [4.328, 51.17, 4.488, 51.27];

// SENSOR CONFIGURATION
