# HCOT sensor backend - API

## Get to '/'

Standard endpoint to see if the server is up and running

## get to '/all'

Returns all the measurement entities in the database

## Get to '/live'

Returns the last measurement for each sensor

## Post to '/all'

Returns the last measurement for each sensor, which took place between the start and end time

expects the following

```
{
  start: Int: unix timestamp,
  end: Int: unix timestamp
}
```

## Post to '/live'

Returns the last measurement for each sensor, which took place between the start and end time

expects the following

```
{
  start: Int: unix timestamp,
  end: Int: unix timestamp
}
```

## Post to '/'

Endpoint to add measurements to the database

expects the following:

```
{
  sensorID: String: id of the sensor,
  decibelValue: Int: measured value,
  decibelTimerange: Int: length of measurement,
  timestamp: Int: unix timestamp,
  geohash: String: geohashed location,
  detected: String: Json object with extra data,
}
```
